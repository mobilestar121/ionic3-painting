import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Config } from '../../../app/app.config';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactComponent {

  constructor(
  	private navCtrl: NavController,
  	private config: Config,
  ) {}

}
