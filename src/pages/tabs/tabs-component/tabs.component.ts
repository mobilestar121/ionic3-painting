import { Component } from '@angular/core';

import { HomeComponent } from '../../home/home-component/home.component';
import { ContactComponent } from '../../contact/contact-component/contact.component';
import { WordpressPosts } from '../../wordpress/wordpress-posts/wordpress-posts.component';
import { WordpressCategories } from '../../wordpress/wordpress-categories/wordpress-categories.component';
import { WordpressFavorites } from '../../wordpress/wordpress-favorites/wordpress-favorites.component';


@Component({
  templateUrl: 'tabs.html'
})
export class TabsComponent {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = WordpressPosts;
  tab2Root: any = WordpressCategories;
  tab3Root: any = WordpressFavorites;
  tab4Root: any = ContactComponent;


  constructor() {

  }
}
