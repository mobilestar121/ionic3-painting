import { Component } from '@angular/core';

import { OnInit } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';

import { WordpressService } from '../shared/services/wordpress.service';
import { WordpressPosts } from '../wordpress-posts/wordpress-posts.component';

@Component({
	templateUrl: './wordpress-categories.html',
	providers: [ WordpressService ]
})
export class WordpressCategories implements OnInit {

	categories: any;
	subcategories: any[];
	showLevel1 = null;
	showLevel2 = null;

	constructor(
		private wordpressService: WordpressService,
		private navController: NavController,
		private loadingController: LoadingController) {}

	ngOnInit() {
		this.getCategories();
	}
	getCategories() {
		let loader = this.loadingController.create({
			content: "Please wait"
		});
		loader.present();
		
		this.wordpressService.getCategories()
		.subscribe(result => {
			this.categories = result;
		},
		error => console.log(error),
    () => loader.dismiss());
	}

	isSubcategories() {
		return this.subcategories.length;
	}

	loadCategory(category) {
		this.navController.push(WordpressPosts, {
			category: category
		});		
	}

	toggleLevel1(idx, category) {
		if (this.isLevel1Shown(idx)) {
			this.showLevel1 = null;
		} else {
			this.showLevel1 = idx;
		}

		let loader = this.loadingController.create({
			content: "Please wait"
		});
		loader.present();
		
		this.wordpressService.getSubcategories(category.ID)
		.subscribe(result => {
			this.subcategories = result;
		},
		error => console.log(error),
    () => loader.dismiss());
	};

	toggleLevel2(idx) {
		if (this.isLevel2Shown(idx)) {
			this.showLevel1 = null;
			this.showLevel2 = null;
		} else {
			this.showLevel1 = idx;
			this.showLevel2 = idx;
		}
	};
	isLevel1Shown(idx) {
		return this.showLevel1 === idx;
	};

	isLevel2Shown(idx) {
		return this.showLevel2 === idx;
	};

}
