import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Config } from '../../../../app/app.config';
import 'rxjs/add/operator/map';

@Injectable()
export class WordpressService {

	constructor(private http: Http, private config: Config) {}

	public login(data) {
		let url = this.config.wordpressApiUrl + '/jwt-auth/v1/token';
		return this.http.post(url, data)
	  	.map(result => {
			return result.json();
		});
	}

	private transformRequest(obj) {
		let p, str;
		str = [];
		for (p in obj) {
			str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
		}
		return str.join('&');
	}

	public getPosts(query) {
		let url = ''
		if(query['keyword']) {
			query = this.transformRequest(query);
		// let url = this.config.wordpressApiUrl + `/wp/v2/posts?${query}&_embed`;
		
     url = this.config.wordpressApiUrl + `/gmpainting.php?${query}`;
		} else {
			query = this.transformRequest(query);
		// let url = this.config.wordpressApiUrl + `/wp/v2/posts?${query}&_embed`;
		
     url = this.config.wordpressApiUrl + `/gmpainting.php?catid=${query}`;
		}
		return this.http.get(url)
	  	.map(result => {
			return result.json();
		});
	}

  public getResults(query) {
    query = this.transformRequest(query);
    // let url = this.config.wordpressApiUrl + `/wp/v2/posts?${query}&_embed`;
    let url = this.config.wordpressApiUrl + `/gmpainting.php?catid=${query}`;
    return this.http.get(url)
      .map(result => {
        return result.json();
      });
  }


	public getPost(id) {
		return this.http.get(this.config.wordpressApiUrl + `/gmpainting.php?id=${id}`)
	  	.map(result => {
			return result.json();
		});
	}

  public getCategories() {
    return this.http.get(this.config.wordpressApiUrl + '/gmpainting.php?op=viewBiCategory')
      .map(result => {
        return result.json();
      });
  }
	public getSubcategories(categoryId) {
    return this.http.get(this.config.wordpressApiUrl + '/gmpainting.php?op=viewBiCategory&id='+categoryId)
      .map(result => {
        return result.json();
      });
  }

	public getMedia(id) {
		return this.http.get(this.config.wordpressApiUrl + `/wp/v2/media/${id}`)
	  	.map(result => {
			return result.json();
		});
	}



	public getTags() {
		return this.http.get(this.config.wordpressApiUrl + '/wp/v2/tags?per_page=100')
		.map(result => {
			return result.json();
		});
	}

	public getPages() {
		return this.http.get(this.config.wordpressApiUrl + '/wp/v2/pages?per_page=100')
		.map(result => {
			return result.json();
		});
	}

  public getPage(id) {
    return this.http.get(this.config.wordpressApiUrl + `/wp/v2/pages/${id}`)
      .map(result => {
        return result.json();
      });
  }

	public getMenus() {
		return this.http.get(this.config.wordpressApiUrl + '/wp-api-menus/v2/menus')
		.map(result => {
			return result.json();
		});
	}

	public getMenu(id) {
		return this.http.get(this.config.wordpressApiUrl + `/wp-api-menus/v2/menus/${id}`)
	  	.map(result => {
			return result.json();
		});
	}

}
